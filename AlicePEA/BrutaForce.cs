﻿using System.Collections.Generic;
using System.Linq;

namespace AlicePEA
{
    class BrutaForce
    {
        Graph graph;
        int bestValue;
        int[] bestPath;

        BrutaForce(Graph graph)
        {
            this.graph = graph;
        }

        public void Run()
        {
            bestValue = int.MaxValue; //Na początku dajemy największą wartość aby pierwsza wyliczona ścieżka przyjeła tą wartość
            var permutationsCount = Factorial(graph.Size);
            var permutations = new int[permutationsCount, graph.Size];

            var cities = new int[graph.Size];
            for (int i = 1; i < graph.Size; i++)
            {
                cities[i] = i;
            }

            //To jest trochę słabe rozwiązanie, bo IEnumerator zwraca List<int>, a nie int[] i trzeba go foreachem przelecieć
            //Czyli samymu trzeba robić licznik. Można niby zamienić na int[], ale trzeba zamienić RotateRight (czyli Swap())
            //Ale nadal trzeba będzie ręcznie przepisywać wartości do tablicy 2-elementowej.
            //Można niby zrobić tablicę list xDDDDDdd
            //Ale najwygodniej zrobić to tak jak ja mam - na listach
            int permCounter = 0;
            foreach (var perm in Permutate(cities.ToList(), graph.Size))
            {
                var valueCounter = 0;
                foreach (var value in perm)
                {
                    permutations[permCounter, valueCounter] = value;
                    valueCounter++;
                }
                permCounter++;
            }

            //Teraz będziemy sprawdzać każdą permutacje i zapiszemy najepszy wynik
            for (int permId = 0; permId < permutationsCount; permId++)
            {
                //Koszt początkowy
                var value = 0;
                //Size - 1, bo ostatnie sprawdzenie będzie dla CityID przedostatniego do ostatneigo.
                for (int cityId = 0; cityId < graph.Size - 1; cityId++) 
                {
                    //Pierwza droga
                    var from = permutations[permId, cityId]; //Id miasta, z którego wychodzimy
                    var to = permutations[permId, cityId + 1]; //Id miasta, do którego idziemy
                    value += graph.Matrix[from, to]; //Dodajemy koszt
                }

                if(value < bestValue)
                {
                    bestValue = value;
                    //W sumie kolejny niepotrzeby hack, który wynika z tego, że używamy tablic i musimy je przepisywać :<
                    for (int i = 0; i < graph.Size; i++)
                    {
                        bestPath[i] = permutations[permId, i];
                    }
                }
            }
        }

        //Rekurencyjna metoda obliczająca silnie!
        int Factorial(int i)
        {
            if (i <= 1)
            {
                return 1;
            }
            return i * Factorial(i - 1);
        }

        //To jest takie dziwo do liczenia permutacji, które zostało zabrane z SO
        //Polega na tym, że za każdym razem jak zostanie wywołana ta "funckja" (czyli enumerator) to zwróci inną wartość.
        //Każda kolejna wartość będzie kolejnyą permutacją, aż się skonczy
        //Wartość jest zwracana metodą "yield return", a po zwróceniu, kod wykonuje się dalej.
        //Ostatnia wartość to będzie jeśli cunt == 1, bo wtedy kod się kończy ;)
        //Próbowałem to jakoś inaczej napisać, ale chyba jestem za głupi, więc na razie to zostanie - chociaż jest mało zrozumiałe.
        public static IEnumerable<List<int>> Permutate(List<int> sequence, int count)
        {
            if (count == 1) yield return sequence;
            else
            {
                for (int i = 0; i < count; i++)
                {
                    foreach (var perm in Permutate(sequence, count - 1))
                        yield return perm;
                    RotateRight(sequence, count);
                }
            }
        }

        /*
        public void testc()
        {
            var tablica = new int[10];

            foreach (var liczba in tablica)
            {
                //liczba
            }

            for (int i = 0; i < tablica.Length; i++)
            {
                //tablica[i]
            }

        }
        */

        public static void RotateRight(List<int> sequence, int count)
        {
            var tmp = sequence[count - 1];
            sequence.RemoveAt(count - 1);
            sequence.Insert(0, tmp);
        }
    }
}
