﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlicePEA
{
    class Graph
    {
        public int[,] Matrix;
        public int Size;

        //Konstruktor, który dostaje string w formie całej macierzy w wszystkimi przerwami, enerami itp.
        Graph(string matrixText)
        {
            //Tworzymy tablicą, która składa się tylko i wyłącznie z stringów tekstu, który był rozdzielony przez wszystkie znaki białe
            var array = matrixText.Split(new string[] { " ", Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries); 
            //stringa zmieniamy na int'a, nie przejmujemy sie tym czy parsowanie się uda, bo zakładamy poprawność danych :D
            //Pierwsza wartość jest rozmiarem.
            Size = int.Parse(array[0]);

            //Inicjujemy tablicę o podanym rozmiarze.
            Matrix = new int[Size, Size];
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    //Tak jak to robiłaś wcześniej, tylko kolejna liczba
                    //j rośnie co interacje, czyli odpowiada za liczby w danym wierszu
                    //i rośnie co Size interacji, czyli odpowiada za kolumny
                    Matrix[i, j] = int.Parse(array[i + j]);
                }
            }
        }
    }
}
